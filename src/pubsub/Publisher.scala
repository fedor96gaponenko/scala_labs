package pubsub

/**
  * Created by fgaponenko on 17.1.19.
  */

class Publisher {
  private var subscribers = List[Subscriber[this.type]]()

  def subscribe(subscriber: Subscriber[this.type]) = {
    subscribers = subscriber :: subscribers
  }

  def pub() = subscribers.foreach(s => s.notify(3))
}
