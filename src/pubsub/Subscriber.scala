package pubsub

/**
  * Created by fgaponenko on 17.1.19.
  */
class Subscriber[T <: Publisher] {

  def notify(event: Int): Unit = {
    println("got an event: " + event)
  }
}
