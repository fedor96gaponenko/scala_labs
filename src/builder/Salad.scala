package builder

/**
  * Created by fgaponenko on 17.1.19.
  */

case class Salad(ingredients: Seq[String])

object Salad {
  sealed trait SaladIngredient

  sealed trait Billet extends SaladIngredient
  sealed trait Cabbage extends SaladIngredient
  sealed trait Topping extends SaladIngredient
  sealed trait Carrot extends SaladIngredient

  type CookedSalad = Billet with Cabbage with Carrot
}