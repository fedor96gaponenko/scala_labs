package builder

import builder.Salad._

/**
  * Created by fgaponenko on 17.1.19.
  */

class SaladBuilder[T <: SaladIngredient] protected(ingredients: Seq[String]) {

  def addCabbage: SaladBuilder[T with Cabbage] = SaladBuilder(ingredients :+ "cabbage")

  def addCarrot: SaladBuilder[T with Carrot] = SaladBuilder(ingredients :+ "carrot")

  def build(implicit ev: T =:= CookedSalad): Salad = Salad(ingredients)
}

object SaladBuilder {

  def apply[T <: SaladIngredient](ingredients: Seq[String]): SaladBuilder[T] = new SaladBuilder[T](ingredients)

  def apply(): SaladBuilder[Billet] = apply[Salad.Billet](Seq())

}
