import builder.SaladBuilder
import pubsub.{Publisher, Subscriber}

/**
  * Created by fgaponenko on 17.1.19.
  */
object Runner {
  def main(args: Array[String]): Unit = {
    val salad = SaladBuilder
      .apply
      .addCarrot
      .addCabbage
      .build

    println(salad)

    val publisher = new Publisher()
    publisher.subscribe(new Subscriber[publisher.type])
    publisher.pub()
  }
}
